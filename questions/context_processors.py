from .models import Question


def trending_questions(request):
    return {'trending_questions': Question.objects.all()[:20]}
