from django import forms
from .models import Question, Answer
from django.utils.translation import ugettext_lazy as _


class AskQuestionForm(forms.ModelForm):

    tags = forms.CharField(required=False,
                           label='Tags',
                           help_text=_('Add up to 3 tags to describe what your question is about'),
                           widget=forms.Textarea(attrs={'placeholder': 'e.g. tag1, tag2, tag3',
                                                        'class': 'form-control', 'rows': 1}),
                           )

    class Meta:
        model = Question
        fields = ('header', 'body',)
        help_texts = {
            'header': _('Be specific and imagine you’re asking a question to another person'),
            'body': _('Include all the information someone would need to answer your question')
        }
        widgets = {
            'header': forms.Textarea(attrs={'placeholder': 'e.g. How to python',
                                            'class': 'form-control', 'rows': 1}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'rows': 8}),
        }

    def clean_tags(self):
        data = self.cleaned_data['tags']
        if not data:
            tags = []
        else:
            tags = [val.strip() for val in data.split(',') if len(val.strip()) > 0]
        if len(tags) > 3:
            raise forms.ValidationError(_("Enter three tags or less"))
        return tags


class AnswerForm(forms.ModelForm):

    class Meta:
        model = Answer

        fields = ('body',)
        widgets = {
            'body': forms.Textarea(attrs={'class': 'form-control', 'rows': 8}),
        }
