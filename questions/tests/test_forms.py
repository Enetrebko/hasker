from django.test import TestCase
from questions.forms import AskQuestionForm, AnswerForm


class AskQuestionFormTest(TestCase):

    def test_empty_header(self):
        data = {'header': '',
                'body': 'Body',
                'tags': 'tag',
                }
        form = AskQuestionForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('This field is required.', form.errors['header'])

    def test_empty_body(self):
        data = {'header': 'Header',
                'body': '',
                'tags': 'tag',
                }
        form = AskQuestionForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('This field is required.', form.errors['body'])

    def test_header_too_long(self):
        data = {'header': 'a' * 301,
                'body': 'Body',
                'tags': 'tag',
                }
        form = AskQuestionForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('Ensure this value has at most', form.errors['header'][0])

    def test_body_too_long(self):
        data = {'header': 'Header',
                'body': 'a' * 1001,
                'tags': 'tag',
                }
        form = AskQuestionForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('Ensure this value has at most', form.errors['body'][0])

    def test_too_much_tags(self):
        data = {'header': 'Header',
                'body': 'Body',
                'tags': 'tag1, tag2, tag3, tag4',
                }
        form = AskQuestionForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('Enter three tags or less', form.errors['tags'])

    def test_valid_form_empty_tags(self):
        data = {'header': 'Header',
                'body': 'Body',
                'tags': '',
                }
        form = AskQuestionForm(data)
        self.assertTrue(form.is_valid())

    def test_valid_form_tags_with_spaces(self):
        data = {'header': 'Header',
                'body': 'Body',
                'tags': 'tag1,  tag2',
                }
        form = AskQuestionForm(data)
        self.assertTrue(form.is_valid())

    def test_valid_form_tags_wo_spaces(self):
        data = {'header': 'Header',
                'body': 'Body',
                'tags': 'tag1,tag2',
                }
        form = AskQuestionForm(data)
        self.assertTrue(form.is_valid())

    def test_valid_form_tags_zero_length(self):
        data = {'header': 'Header',
                'body': 'Body',
                'tags': 'tag1,tag2,,,tag3,,',
                }
        form = AskQuestionForm(data)
        self.assertTrue(form.is_valid())


class AnswerFormTest(TestCase):

    def test_empty_body(self):
        data = {'body': ''}
        form = AnswerForm(data)
        self.assertFalse(form.is_valid())
        self.assertIn('This field is required.', form.errors['body'])

    def test_valid_form(self):
        data = {'body': 'body'}
        form = AnswerForm(data)
        self.assertTrue(form.is_valid())
