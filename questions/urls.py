from django.urls import path
from .views import IndexView, AskQuestionView, QuestionView, VoteView, MarkCorrectAnswerView, SearchResultsView


app_name = 'questions'
urlpatterns = [
    path('order_by/<str:orderby>', IndexView.as_view(), name='index'),
    path('new/', AskQuestionView.as_view(), name='ask'),
    path('question/<int:pk>/', QuestionView.as_view(), name='question'),
    path('vote_up_<str:object_type>_<int:obj_id>/', VoteView.as_view(vote_up=True), name='vote_up'),
    path('vote_down_<str:object_type>_<int:obj_id>/', VoteView.as_view(vote_up=False), name='vote_down'),
    path('mark_<int:answer_id>_<int:question_id>/', MarkCorrectAnswerView.as_view(), name='correct_answer'),
    path('search/', SearchResultsView.as_view(), name='search_results'),
]
