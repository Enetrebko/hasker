from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, FormView
from .forms import AskQuestionForm, AnswerForm
from .models import Question, Answer, Tag
from django.db.models import Q
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.core.mail import send_mail
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.db import transaction


class AskQuestionView(LoginRequiredMixin, CreateView):

    form_class = AskQuestionForm
    model = Question
    login_url = reverse_lazy('users:login')
    template_name = 'ask_question.html'

    @transaction.atomic()
    def form_valid(self, form):
        form.instance.user = self.request.user
        form.save()
        tags = form.cleaned_data.get('tags')
        tags_objects = [Tag.objects.get_or_create(tag_text=tag)[0] for tag in tags]
        form.instance.tags.add(*tags_objects)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('questions:question', kwargs={'pk': self.object.id})


class QuestionView(ListView, FormView):

    paginate_by = 20
    form_class = AnswerForm
    context_object_name = 'answers'
    template_name = 'question.html'

    def get_queryset(self):
        return Answer.objects.filter(question=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = AnswerForm()
        context['question'] = get_object_or_404(Question, pk=self.kwargs['pk'])
        return context

    def send_notification(self, question):
        email_header = 'New answer'
        email_body = '''You've got new answer: '''
        send_mail(
            email_header,
            "".join([email_body, self.request.build_absolute_uri(question.get_absolute_url())]),
            settings.EMAIL_HOST_USER,
            [question.user.email],
            fail_silently=False)

    @transaction.atomic()
    def form_valid(self, form):
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        form.instance.user = self.request.user
        form.instance.question = question
        form.instance.answers = question.answers + 1
        form.save()
        self.send_notification(question)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('questions:question', kwargs={'pk': self.kwargs['pk']})


class IndexView(ListView):

    model = Question
    paginate_by = 30
    template_name = 'index.html'

    def get_ordering(self):
        ordering = self.kwargs.get('orderby', '-created_date')
        return ordering


class VoteView(LoginRequiredMixin, View):

    login_url = reverse_lazy('users:login')
    vote_up = False

    def get(self, request, *args, **kwargs):
        objects = {
            'answer': Answer,
            'question': Question,
        }
        object_type = self.kwargs.get('object_type')
        obj_id = self.kwargs.get('obj_id')
        vote_up = self.vote_up
        request_object = get_object_or_404(objects[object_type], pk=obj_id)
        question = request_object if object_type == 'question' else request_object.question
        already_voted_up = request_object.users_voted_up.filter(id=self.request.user.id).exists()
        already_voted_down = request_object.users_voted_down.filter(id=self.request.user.id).exists()
        if vote_up:
            if already_voted_down:
                request_object.votes += 1
                request_object.users_voted_down.remove(self.request.user)
            elif not already_voted_up:
                request_object.votes += 1
                request_object.users_voted_up.add(self.request.user)
        else:
            if already_voted_up:
                request_object.votes -= 1
                request_object.users_voted_up.remove(self.request.user)
            elif not already_voted_down:
                request_object.votes -= 1
                request_object.users_voted_down.add(self.request.user)
        request_object.save()
        return redirect(reverse_lazy('questions:question', kwargs={'pk': question.id}))


class MarkCorrectAnswerView(LoginRequiredMixin, View):

    login_url = reverse_lazy('users:login')

    def get(self, request, *args, **kwargs):
        question = get_object_or_404(Question, pk=self.kwargs.get('question_id'))
        if question.user == request.user:
            answer = get_object_or_404(Answer, pk=self.kwargs.get('answer_id'))
            if question.correct_answer == answer:
                question.correct_answer = None
            else:
                question.correct_answer = answer
            question.save()
        return redirect(reverse_lazy('questions:question', kwargs={'pk': question.id}))


class SearchResultsView(ListView):

    model = Question
    template_name = 'search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q').strip()
        if query.startswith('tag:'):
            query = query.replace('tag:', '').strip()
            object_list = Question.objects.filter(tags__tag_text=query)
        else:
            object_list = Question.objects.filter(
                Q(header__icontains=query) | Q(body__icontains=query)
            )
        return object_list
