from django.apps import AppConfig


class HaskerAppConfig(AppConfig):
    name = 'users'
