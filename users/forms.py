from django import forms
from .models import UserProfile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm, UsernameField


class SignUpForm(UserCreationForm):

    username = forms.CharField(label='Login',
                               max_length=100,
                               widget=forms.TextInput(attrs={'class': 'form-control'}),
                               )

    email = forms.EmailField(label='Email',
                             max_length=100,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}),
                             )

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                )
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)


class CustomAuthenticationForm(AuthenticationForm):

    username = UsernameField(label='Login',
                             widget=forms.TextInput(attrs={'autofocus': True, 'class': 'form-control'}),
                             )
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                               )


class ChangeProfileForm(forms.ModelForm):

    email = forms.EmailField(label='Email',
                             max_length=100,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}),
                             )

    class Meta:
        model = User
        fields = ('email',)


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ('avatar',)
