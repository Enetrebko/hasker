from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class UserProfile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='pictures/', blank=True)

    @property
    def image_url(self):
        if self.avatar:
            return self.avatar.url
        return settings.STATIC_URL + 'img/default.jpeg'
