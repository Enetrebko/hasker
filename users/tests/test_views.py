from django.test import TestCase
from django.urls import reverse_lazy, reverse
from users.forms import *
from users.models import *


class SignUpView(TestCase):
    def test_get(self):
        response = self.client.get(reverse('users:signup'))
        self.assertEqual(response.status_code, 200)
        template_names = [template.name for template in response.templates]
        self.assertIn('base.html', template_names)
        self.assertIn('signup.html', template_names)
        self.assertIn('_navbar.html', template_names)
        self.assertIn('_sidebar.html', template_names)
        self.assertIsInstance(response.context['user_form'], SignUpForm)
        self.assertIsInstance(response.context['user_profile_form'], UserProfileForm)

    def test_post_redirect(self):
        response = self.client.post(reverse_lazy('users:signup'),
                                    data={'username': 'testuser',
                                          'email': 'aa@bb.cc',
                                          'password1': 'pwd123456',
                                          'password2': 'pwd123456'})
        self.assertRedirects(response, reverse_lazy("index"))


class CustomLoginView(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='testuser')
        self.user.set_password('pwd123456')
        self.user.save()

    def test_get(self):
        response = self.client.get(reverse('users:login'))
        self.assertEqual(response.status_code, 200)
        template_names = [template.name for template in response.templates]
        self.assertIn('base.html', template_names)
        self.assertIn('login.html', template_names)
        self.assertIn('_navbar.html', template_names)
        self.assertIn('_sidebar.html', template_names)
        self.assertIsInstance(response.context['form'], CustomAuthenticationForm)

    def test_post_redirect(self):
        response = self.client.post(reverse_lazy('users:login'),
                                    data={'username': 'testuser', 'password': 'pwd123456'})
        self.assertRedirects(response, reverse_lazy("index"))


class ChangeProfileView(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='testuser')
        self.user.set_password('pwd123456')
        self.user.save()
        self.userprofile = UserProfile.objects.create(user=self.user)

    def test_not_auth_user_redirect(self):
        response = self.client.get(reverse_lazy("users:settings"))
        self.assertRedirects(response, "".join([str(reverse_lazy("users:login")),
                                                "?next=",
                                                str(reverse_lazy("users:settings"))]))

    def test_auth_user_get(self):
        self.client.login(username='testuser', password='pwd123456')
        response = self.client.get(reverse('users:settings'))
        self.assertEqual(response.status_code, 200)
        template_names = [template.name for template in response.templates]
        self.assertIn('base.html', template_names)
        self.assertIn('settings.html', template_names)
        self.assertIn('_navbar.html', template_names)
        self.assertIn('_sidebar.html', template_names)
        self.assertIsInstance(response.context['user_form'], ChangeProfileForm)
        self.assertIsInstance(response.context['user_profile_form'], UserProfileForm)
        self.assertEqual(response.context['user_form'].instance, self.user)
        self.assertEqual(response.context['user_profile_form'].instance, self.userprofile)

    def test_post_redirect(self):
        self.client.login(username='testuser', password='pwd123456')
        response = self.client.post(reverse_lazy('users:settings'),
                                    data={'email': 'aa@bb.cc'})
        self.assertRedirects(response, reverse_lazy("index"))


class LogoutTest(TestCase):
    def test_get(self):
        response = self.client.get(reverse('users:logout'))
        self.assertRedirects(response, reverse_lazy("index"))
