from django.urls import path
from .views import SignUpView, CustomLoginView, ChangeProfileView
from django.contrib.auth.views import LogoutView


app_name = 'users'
urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('login/', CustomLoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(next_page='index'), name='logout'),
    path('settings/', ChangeProfileView.as_view(), name='settings'),
]
