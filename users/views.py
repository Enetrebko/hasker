from django.shortcuts import render
from django.views.generic.base import View
from .forms import SignUpForm, UserProfileForm, CustomAuthenticationForm, ChangeProfileForm
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.db import transaction


class SignUpView(View):

    user_form = SignUpForm
    user_profile_form = UserProfileForm
    template_name = 'signup.html'

    def get(self, request):
        user_form = self.user_form()
        user_profile_form = self.user_profile_form()
        return render(request, self.template_name,
                      context={"user_form": user_form,
                               "user_profile_form": user_profile_form})

    @transaction.atomic()
    def post(self, request):
        user_form = self.user_form(request.POST)
        user_profile_form = self.user_profile_form(request.POST, request.FILES)
        if user_form.is_valid() and user_profile_form.is_valid():
            user_form.save()
            user_profile = user_profile_form.save(commit=False)
            user_profile.user_id = user_form.instance.id
            user_profile.save()
            username, password = user_form.cleaned_data.get('username'), user_form.cleaned_data.get('password1')
            new_user = authenticate(username=username, password=password)
            login(self.request, new_user)
            return redirect(reverse_lazy('index'))
        return render(request, self.template_name,
                      context={"user_form": user_form,
                               "user_profile_form": user_profile_form})


class CustomLoginView(LoginView):

    authentication_form = CustomAuthenticationForm
    template_name = 'login.html'


class ChangeProfileView(LoginRequiredMixin, View):

    user_form = ChangeProfileForm
    user_profile_form = UserProfileForm
    login_url = reverse_lazy('users:login')
    template_name = 'settings.html'

    def get(self, request):
        user_form = self.user_form(instance=request.user)
        user_profile_form = self.user_profile_form(instance=request.user.userprofile)
        return render(request, self.template_name,
                      context={"user_form": user_form,
                               "user_profile_form": user_profile_form})

    @transaction.atomic()
    def post(self, request):
        user_form = self.user_form(request.POST, instance=request.user)
        user_profile_form = self.user_profile_form(request.POST, request.FILES, instance=request.user.userprofile)
        if user_form.is_valid() and user_profile_form.is_valid():
            user_form.save()
            user_profile_form.save()
            return redirect(reverse_lazy('index'))
        return render(request, self.template_name,
                      context={"user_form": user_form,
                               "user_profile_form": user_profile_form})
